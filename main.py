from config import BUILD, SRC
from pathlib import Path
import subprocess
from decks import Deck
import shutil


# ---- Deck initialization and functions ----
decks = sorted([Deck(path) for path in SRC.iterdir() if SRC.is_dir()],
               key=lambda d: d.name)


def name_to_index(string, obj):
    '''
    Checks if a string corresponds to a deck name and returns the deck index
    if it is and None otherwise.
    If it is a valid selection number it will return the corresponding index.
    '''
    if string:
        if string.isdigit() and int(string) > 0 and\
                                int(string) <= len(obj):
            return int(string) - 1
        else:
            for i in range(len(obj)):
                if string.lower() == obj[i].name.lower():
                    return i
    return None


MENU = f'''Searching for cards in {SRC}\n
Select deck:
'''

shutil.copyfile("empty.pdf", BUILD / Path("flashcard.pdf"))
viewer = subprocess.Popen(["zathura",
                           fr"{BUILD}/flashcard.pdf"])

# Program status variables
_open = True
status = 0
user_input = None
deck_index = None
active_deck = None
card_index = None
active_card = None

# ---- Status functions ----


def main_menu(user_input):
    global status
    global active_deck
    deck_index = name_to_index(user_input, decks)
    if deck_index is not None:
        active_deck = decks[deck_index]
        active_deck.load()
        status = 1
        return 1
    else:
        print(MENU)
        for i in range(len(decks)):
            print(f'{i+1}. {decks[i].name}')


def deck_menu(user_input):
    global status
    global active_card
    if not active_deck.cards:
        status = 0
        return 1
    active_card = active_deck.cards[0]
    active_card.compile(BUILD)
    status = 2
    return 1


def view_card(user_input):
    global status
    input("[Press enter to view solution]")
    active_card.compile(BUILD, True)
    status = 3
    return 1


def grade_card(user_input):
    global status
    print("Grade your knowledge of this card from 0 to 5:")
    if user_input is not None and\
            user_input.isdigit() and\
            int(user_input) >= 0 and\
            int(user_input) <= 5:
        active_card.update_par(int(user_input))
        active_deck.cards.pop(0)
        status = 1
        return 1


menus = [main_menu, deck_menu, view_card, grade_card]

# ---- Main while loop ----

while _open:
    print("\033c", end='')  # clear console
    print("#### TEXCARDS ####\n")

    if menus[status](user_input):
        user_input = None
        continue

    user_input = input("\n>")
    if user_input == "exit" or user_input == "q":
        viewer.terminate()
        _open = False
