import subprocess
from pathlib import Path
from datetime import date


class Card:
    def __init__(self, path):
        self.path = path
        self.name = path.stem
        text = self.path.read_text()
        # Writes a comment at the start of the file to contain flashcard date.
        if text.find("%BEGIN") == -1:
            datacomment = '%0\n%2.5\n%1\n%0001-01-01\n%BEGIN\n'
            path.write_text(datacomment + text)

    def compile(self, output, solution=False):
        text = self.path.read_text()
        if not solution:
            text = text[:text.find(r"\newpage")]
        result = r'''\documentclass{article}
        \usepackage{amsmath,amssymb}

        \pagestyle{empty}

        \begin{document}
        %s
        \end{document}
        ''' % text

        tex = Path(self.path.parent / Path("flashcard.tex"))
        tex.write_text(result)
        subprocess.Popen(["pdflatex",
                          r"-output-directory=build",
                          "-jobname=flashcard",
                          tex],
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def read_par(self):
        '''
        Retrieves parameters from the start of the flashcard .tex file,
        returns (n, EF, I).
        n = repetition number
        EF = easiness factor
        IR = inter-repetition interval
        LR = date of the last review
        '''
        text = self.path.read_text()
        data = text[:text.find("\n%BEGIN")]
        data = data.replace("%", "").split("\n")
        n, EF, IR = int(data[0]), float(data[1]), int(data[2])
        LR = date.fromisoformat(data[3])
        return (n, EF, IR, LR)

    def update_par(self, q):
        '''
        Updates flashcard parameters based on user grade q.
        '''
        n, EF, IR, LR = self.read_par()
        if q >= 3:
            if n == 0:
                IR = 1
            elif n == 1:
                IR = 6
            else:
                IR = round(IR * EF)
            n += 1
        else:
            n = 0
            IR = 1

        EF = EF + (0.1 - (5 - q) * (0.08 + (5 - q) * 0.02))
        if EF < 1.3:
            EF = 1.3

        LR = date.today()
        self.write_par((n, EF, IR, LR))

        return (n, EF, IR, LR)

    def write_par(self, par):
        n, EF, IR, LR = par
        datacomment = f"%{n}\n%{EF}\n%{IR}\n%{LR}\n"
        text = self.path.read_text()
        src = text[text.find("%BEGIN\n"):]
        self.path.write_text(datacomment + src)

    def get_interval(self):
        LR = self.read_par()[3]
        return (date.today() - LR).days


class Deck:
    def __init__(self, path):
        self.path = path
        self.name = path.stem

        self.cards = []
        self.all_cards = []

    def load(self):
        (self.path / Path("flashcard.tex")).unlink(missing_ok=True)
        self.all_cards = self.all_cards +\
            [Card(p) for p in self.path.iterdir()]
        for card in self.all_cards:
            if card.get_interval() >= card.read_par()[2]:
                self.cards.append(card)
